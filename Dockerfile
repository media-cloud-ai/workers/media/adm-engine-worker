FROM rust:1.50.0-slim-buster as builder

RUN apt-get update && \
    apt-get install -y \
      libssl-dev \
      pkg-config \
      git \
      g++ \
      make \
      cmake \
      libboost-dev

RUN git clone --recursive https://github.com/ebu/libear.git && \
    cd libear/ && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j && \
    make install

RUN git clone --branch 0.11.0 https://github.com/IRT-Open-Source/libadm.git && \
    cd libadm && \
    git checkout 0.11.0 && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j && \
    make install

RUN git clone --branch 0.10.0 https://github.com/IRT-Open-Source/libbw64.git && \
    cd libbw64 && \
    git checkout 0.10.0 && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j && \
    make install

ADD . ./adm_engine_worker

RUN cd adm_engine_worker && \
    cargo build --release && \
    cargo install --path . --root /usr/local

FROM debian:buster

RUN apt-get update && \
    apt-get install -y \
      libssl-dev

COPY --from=builder //usr/local/bin/adm-engine-worker /usr/local/bin/
COPY --from=builder /usr/local/lib/libadm.a /usr/local/lib/
COPY --from=builder /usr/local/lib/libear.a /usr/local/lib/
COPY --from=builder /usr/local/include/adm/ usr/local/include/adm/
COPY --from=builder /usr/local/include/ear/ usr/local/include/ear/
COPY --from=builder /usr/local/include/bw64/ usr/local/include/bw64/

ENV AMQP_QUEUE job_adm_engine

CMD adm-engine-worker
