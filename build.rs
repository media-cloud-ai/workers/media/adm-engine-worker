extern crate built;

use std::path::Path;

fn build_adm_engine_library() {
  let adm_include_path = std::env::var("ADM_INCLUDE_PATH").unwrap_or_default();
  let ear_include_path = std::env::var("EAR_INCLUDE_PATH").unwrap_or_default();
  let bw64_include_path = std::env::var("BW64_INCLUDE_PATH").unwrap_or_default();

  let sub_module_path = Path::new("./adm_engine");
  let include_path = sub_module_path.join("src");
  let source_path = include_path.join("adm_engine");

  let source_files = std::fs::read_dir(source_path.clone()).unwrap();

  let cpp_files: Vec<String> = source_files
    .into_iter()
    .filter(|entry| entry.is_ok())
    .map(|entry| entry.unwrap().path().to_str().unwrap().to_string())
    .filter(|source_file_path| source_file_path.ends_with(".cpp"))
    .collect();

  cc::Build::new()
    .files(cpp_files)
    .cpp(true)
    .include(include_path.to_str().unwrap())
    .include(adm_include_path)
    .include(ear_include_path)
    .include(bw64_include_path)
    .compile("admengine");
}

fn main() {
  built::write_built_file().expect("Failed to acquire build-time information");

  // Unix OS dependant code:
  let library_path = std::env::var("LD_LIBRARY_PATH").unwrap_or_default();
  library_path
    .split(':')
    .for_each(|lib_path| println!("cargo:rustc-link-search={}", lib_path));

  build_adm_engine_library();
}
